package com.example.helloandroid;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	int i = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		Button btn = (Button) findViewById(R.id.button1);
		
		btn.setOnClickListener(new Button.OnClickListener(){
		
			/* (non-Javadoc)
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TextView txt = (TextView) findViewById(R.id.textView1);
//				txt.setText("abc");
				i = i+1;
				Random rnd = new Random();
				int rndNum = rnd.nextInt(2);
				
				if (rndNum == 0) {
					String sti = String.valueOf(i);
					txt.setText(sti);
				}
				else{
					txt.setText("押し過ぎ");
				}
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
